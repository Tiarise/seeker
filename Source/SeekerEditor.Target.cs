using UnrealBuildTool;

public class SeekerEditorTarget : TargetRules
{
	public SeekerEditorTarget( TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V5;
		ExtraModuleNames.AddRange(new string[] { "SeekerCore" });
	}
}