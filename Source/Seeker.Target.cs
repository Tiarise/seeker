using UnrealBuildTool;

public class SeekerTarget : TargetRules
{
	public SeekerTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V5;
		ExtraModuleNames.AddRange(new string[] { "SeekerCore"});
	}
}