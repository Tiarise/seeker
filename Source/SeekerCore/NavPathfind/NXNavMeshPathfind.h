﻿#pragma once

#include "CoreMinimal.h"
#include "Misc/Optional.h"
#include "NavMesh/NavMeshBoundsVolume.h"
#include "NavigationData.h" //FPathFindingResult
#include "NXNavMeshPathfind.generated.h"

/*
	Here we gather the methods of
	smoothing we created.
	TODO: In time, I will create more for science!
*/
UENUM()
enum class ESmoothingMethod : uint8
{
	CRSplineSmoothing, //Catmull-Rom spline interpolation.
};

/*
	Catmull-Rom spline variant
	selector.
*/
UENUM()
enum class ECRSplineVariant : uint8
{
	Uniform,
	Centripetal,
	Chordal
};

/*
	The state of the NavMeshPath we are building.
	This is needed, as the smoothing process can take
	up multiple ticks.
*/
UENUM()
enum class ENXNavPathDataState : uint8
{
	UnInitialized,
	Initialized,
	InitialTurn,
	SplineSmoothing,
	Finished
};

/*
	Structure for storing the state of the
	path and current settings for the build process.
*/
USTRUCT()
struct FNXNavMeshPathData
{
	GENERATED_BODY()

	//General Data
	ENXNavPathDataState State = ENXNavPathDataState::UnInitialized;
	TWeakObjectPtr<AActor> NavActor = nullptr;

	TArray<FNavigationPortalEdge> PortalEdges;
	TArray<FVector> StringPulledPath;
	TArray<FVector> BasePath;
	int NextBPIdx = -1;
	
	TArray<FVector> ProcessedPath;
	TArray<FVector> SubSegmentCache;
	
	//Initial Turn State Data
	float CurrentTurnAngle = 0.0f;
	float TurnAngleDone = 0.0f;
	int SidesChecked = 1;

	//CRSplineSmoothing Data
	bool SmoothedInitialTurnEnd = false;
	float CurrentTension = 0.0f;
	float CRVariant = 0.0f;
};

/*
	Structure for user settings in the details panel.
	With this the user can fine-tune the pathfind.
*/
USTRUCT()
struct FNXNavMeshPathSettings
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = "Base Settings", meta = (ClampMin = "1"))
	int OperationCyclesPerCall = 5;
	UPROPERTY(EditAnywhere, Category = "Base Settings")
	ESmoothingMethod SmoothingMethod;	
	UPROPERTY(EditAnywhere, Category = "Base Settings", meta = (ClampMin = "0"))
	float MaximumDistanceWithoutWaypoint = 500.0f;
	UPROPERTY(EditAnywhere, Category = "Base Settings", meta = (ClampMin = "0.1"))
	float MaximumDistanceAllowedFromOriginalWaypoint = 200.0f;
	UPROPERTY(EditAnywhere, Category = "Base Settings", meta = (ClampMin = "0"))
	FVector MaximumAdjustExtentFromEndPoint = FVector(1000.0f,1000.0f,500.0f);
	
	//Settings for InitialTurn turn phase.
	UPROPERTY(EditAnywhere, Category = "InitialTurn", meta = (ClampMin = "5"))
	float InitialTurnStepDistance = 25.0f;
	UPROPERTY(EditAnywhere, Category = "InitialTurn", meta = (ClampMin = "5"))
	float InitialTurnMinimumAngle = 10.0f; //Incr this to make bigger 
	UPROPERTY(EditAnywhere, Category = "InitialTurn", meta = (ClampMin = "1", ClampMax = "90"))
	float InitialTurnMaximumAngle = 35.0f;
	UPROPERTY(EditAnywhere, Category = "InitialTurn", meta = (ClampMin = "5"))
	float InitialTurnAngleStep = 5.0f;
	UPROPERTY(EditAnywhere, Category = "InitialTurn", meta = (ClampMax = "90"))
	float InitialTurnArrivalAngleMaximum = 90.0f;

	//Settings for the CRSplineSmoothing phase.
	UPROPERTY(EditAnywhere, Category = "CRSplineSmoothing", meta = (ClampMin = "0", ClampMax = "1"))
	float BaseTension = 0.0f;
	UPROPERTY(EditAnywhere, Category = "CRSplineSmoothing", meta = (ClampMin = "0", ClampMax = "1"))
	float TensionIncPerFailure = 0.2f;
	UPROPERTY(EditAnywhere, Category = "CRSplineSmoothing")
	ECRSplineVariant CRSplineVariant = ECRSplineVariant::Centripetal;	
};

/*
	Structure for storing the settings for customising the
	debug draw for the path.
*/
USTRUCT()
struct FNXPathDrawSettings
{
	GENERATED_BODY()
	
	UPROPERTY(EditAnywhere)
	float Duration = 3.0f;
	UPROPERTY(EditAnywhere)
	float WaypointsZOffset = 5.0f;
	UPROPERTY(EditAnywhere)
	FColor LineColor = FColor::Blue;
	UPROPERTY(EditAnywhere)
	bool ShowWaypoints;
	UPROPERTY(EditAnywhere)
	FColor WaypointColor = FColor::Yellow;
};

/*
	A static class, that holds the most important functions for the NavigationMesh based
	pathfinding and the smoothing system.
*/
class NXNavMeshPathFind
{
public:
	static TOptional<FVector> ProjectPointToNavigation(const AActor* WorldContextObject, const FVector& Point, const FVector& AgentExtent,
		const FVector& AdjustExtent = FVector::ZeroVector);	
	static FPathFindingResult FindPathSync(const AActor* NavAgent, const FVector& StartPoint, const FVector& EndPoint, const FVector& AgentExtent,
		const FVector& AdjustExtent = FVector::ZeroVector);
	
	static TArray<FNavigationPortalEdge> GetPortalEdgesCrossed(const FNavPathSharedPtr& Path);
	static bool NavPathRaycast(AActor* WorldContextObject, const FVector& StartPoint, const FVector& EndPoint, FVector* OutHitLocation = nullptr);

	static void InitializeNavMeshPathData(FNXNavMeshPathData& Data, const FNXNavMeshPathSettings& Settings, const TArray<FVector>& StringPulledPath,
		const TArray<FNavigationPortalEdge>& PortalEdges, AActor* NavActor);
	static void IteratePathfind(FNXNavMeshPathData& Data, const FNXNavMeshPathSettings& Settings);

	static void CreateBasePath(FNXNavMeshPathData& Data, const FNXNavMeshPathSettings& Settings);
	static void ApplyInitialTurn(FNXNavMeshPathData& Data, const FNXNavMeshPathSettings& Settings);
	static void ApplySplineSmoothing(FNXNavMeshPathData& Data, const FNXNavMeshPathSettings& Settings);
	
	static void DebugDrawPath(const AActor* WorldContextObject, FNXPathDrawSettings& Data, const TArray<FVector>& Path);
	static float GetTurnRequiredToTargetPoint(const FVector& ForwardVector, const FVector& OriginPoint, const FVector& TargetPoint);
	static int GetNextWaypointBasedOnLOS(const FVector& StartPoint, FNXNavMeshPathData& Data);
};

/*
	A derived class of the ANavMeshBoundsVolume, that can display the built path without running
	the game, so the iteration can be faster. Can be manually initiated by ShowPathTest. Or you may
	automate it with LiveUpdateInterval, this way you can observe changes as you move/turn things around.

    Please note that placed obstacles possibly will need a NavModifierVolume inside them. So you can
    prevent the creation of in obstacle navigation areas!
	
	Press P for showing navmesh in the editor.
	Also, if you want to see poly edges: ProjectSettings/NavigationMesh/DrawPolyEdges.
*/
UCLASS()
class SEEKERCORE_API ANXNavMeshBoundsVolume : public ANavMeshBoundsVolume
{
	GENERATED_BODY()

public:
	ANXNavMeshBoundsVolume();
	virtual void Tick(float DeltaSeconds) override;
	virtual bool ShouldTickIfViewportsOnly() const override;

private:
	UFUNCTION(CallInEditor, Category = "Pathfind")
	void ShowPathTest();
	
	UPROPERTY(EditAnywhere, Category = "Pathfind")
	float LiveUpdateInterval = 0.0f; //For demonstration purposes.
	
	UPROPERTY(EditAnywhere, Category = "Pathfind|Base Settings")
	AActor* StartPoint;
	UPROPERTY(EditAnywhere, Category = "Pathfind|Base Settings")
	AActor* EndPoint;

	UPROPERTY(EditAnywhere, Category = "Pathfind|Base Settings")
	FNXNavMeshPathSettings PathSettings;
	UPROPERTY(EditAnywhere, Category = "Pathfind|Base Settings")
	FNXPathDrawSettings SmoothedPathDrawData;
	UPROPERTY(EditAnywhere, Category = "Pathfind|Base Settings")
	FNXPathDrawSettings BasePathDrawData;
	UPROPERTY(EditAnywhere, Category = "Pathfind|Base Settings")
	FNXPathDrawSettings StringPulledPathDrawData;
	
	//Variables needed by the implementation
	FNXNavMeshPathData PathData;
	float LiveUpdateCountdown = 0;
};