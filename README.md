# Seeker

Custom path smoothing solutions built on top of the Unreal Engine Navmesh system.<br>
In this project I'm gathering path smoothing solutions I find interesting and try them out next to each other.

<p align="center">
  <img src="./MediaForReadme/PathfindCRS.gif" alt="Pathfind demonstration">
</p>

<p align="center">
Red line: String pulled path. ( The one generated by Unreal. )<br>
Blue line:  Smoothed path.
</p>

## Current Smoothing Solutions
- Catmull-Rom Spline Interpolation ( Uniform, Centripetal, Chordal  )

## Design time testing
This implementation can create paths without a running game. This greatly improves the iteration times.<br>
Look for the NXNavMeshBoundsVolume in the example scene and edit the Pathfind category for settings.<br>
Move StartPoint/EndPoint actors for testing different scenarios.<br>
Path generation can be initiated by the Show Path Test button or there is a Live Update solution as well.
<p align="right">
  <img src="./MediaForReadme/PathfindSettings.png" alt="Pathfind demonstration">
</p>

## Requirements
 - The project is using Unreal Engine 5.4
 - Since the project was created with a locally compiled Unreal Engine the EngineAssociation in the UProject file most likely will have to be changed. 



